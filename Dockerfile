FROM node:14.17.6-alpine
WORKDIR '/demo-app'

COPY package.json ./
RUN npm install --registry=https://registry.npm.taobao.org 

COPY . .
CMD ["npm","start"]
